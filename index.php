<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <style type="text/css">
        div{
          margin-top: 80px;
        }
        .top-button a:link, a:visited {
        width: 200px;
        background-color:#eba210;
        color: #f1f1f1;
        padding: 10px 15px;
        font-size: 20px;
        font-weight: bold;
        border-radius:3px;
        border:1px solid #eb8510;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        margin-bottom: 40px;
      }
      .top-button a:hover, a:active {
        background-color: #eba910;
      }
    </style>
  </head>
<body>
<h3>REGISTER:</h3>
<?php
if(isset($_SESSION["message"])) {
    echo $_SESSION["message"];
}
?>
  <form name="myform" action="post.php" method="post" enctype="multipart/form-data">
    <label for="student_name">Student Name:</label>
    <input id="student_name" type="text" name="name"><br>
    <label for="email">Student Email: </label>
    <input id="email" type="email" name="email"><br>
    Gender:<br><label for="male">Male</label><input id="male" type="radio" name="gender" value="male">
    Female<input type="radio" name="gender" value="female"><br>
    Picture:<input type="file" name="image" id="image"><br>
    DOB:<label for="birthday"></label>
    <input type="date" id="birthday" name="date"><br>
    Bio:<textarea name="textarea"></textarea><br>
    <input type="submit" value="SUBMIT" name="submit"><br>
    <div class="top-button">
      <a href="../get-data-to-csv-file-list-view/listview.php">View all profiles</a>
    </div>
</form>
</body>
</html>